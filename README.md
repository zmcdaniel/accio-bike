# README #

A registry for your bicycle to proactively prevent and recover from bike thefts.

### What is this repository for? ###

Without creating an account, a user can search the database of missing bikes for information like a bicycle's unique serial number, brand, model, color, size, or the zip code from which it went missing. 

If your bike has gone missing, create a free account and fill in your bike's information. 

Searching Craigslist for a used bike to purchase? Check our missing registry to ensure you're not buying someone else's stolen property.

### Technology Used ###

* Ruby
* Rails
* Cloudinary
* Sass
* jQuery
* SVG
* CoffeeScript
* PostgreSQL (via Interactive Ruby Shell)
* Active Record
* Bootstrap
* Font Awesome Icons
* Material UI library and framework

### User Stores ###

* As a bike purchaser, I want to be able to check to see if the bike I'm about to purchase on Craigslist is stolen property
* As a bike owner, I want to proactively add my bike to a registry so I can recover it quickly if stolen.
* As a law enforcement officer, I want to reference a missing bike index to return a recently recovered bicycle.
* As a bike owner whose bicycle recently stolen, I want to post my bike on a public forum to increase visibility and possibility of recovery.
* As a person who recently found an abandoned bike, I want to know if this is a missing bike and how to return it.

### Wireframes and UI Concept ###

[Album Here](http://imgur.com/a/IwWNA)